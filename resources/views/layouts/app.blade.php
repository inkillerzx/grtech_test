<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Timeline</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
        @include('layouts.styles')
        @stack('styles')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @stack('modals')
            <header class="main-header">
                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{url('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                                    <span class="hidden-xs">{{ucfirst(Auth::user()->name) ?? 'Guest'}}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                        <p>
                                            {{ucfirst(Auth::user()->name) ?? 'Guest'}} - Web Developer
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();$('#loading').show();">Sign out</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>{{ucfirst(Auth::user()->name) ?? 'Guest'}}</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu" data-widget="tree">
                        @include('layouts.menu')
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                @if(View::hasSection('content-header'))
                    <section class="content-header">
                        @yield('content-header')
                    </section>
                @else
                    <section class="content-header">
                        <h1>
                            Welcome to GRTech Test
                            <small>Laravel</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        </ol>
                    </section>
                @endif
                @if(View::hasSection('main-content'))
                    <section class="content">
                        @if(View::hasSection('main-content'))
                            <div class="row">
                                <div class="col-md-12">
                                    @yield('main-content')
                                </div>
                            </div>
                        @endif
                    </section>
                @endif
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.13
                </div>
                <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE</a>.</strong> All rights
                reserved.
            </footer>
            <div class="control-sidebar-bg"></div>
        </div>
        @include('layouts.scripts')
        @stack('scripts')
    </body>
</html>
