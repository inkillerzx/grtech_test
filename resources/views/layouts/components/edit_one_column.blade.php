<div class='col-md-12 form-group'>
    <div class="row">
        <div class='col-md-2'>
            <label for="@if(isset($for)){{ $for }}@endif">@if(isset($label)){{ $label }}@endif @if(in_array($required,[1,'required']))<span class="required"></span>@endif</label>
        </div>
        <div class='col-md-10 col-input'>
            @if(isset($field)){{ $field }}@endif
        </div>
    </div>
</div>