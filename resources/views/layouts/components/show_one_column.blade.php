<div class='col-md-12'>
    <div class="row">
        <div class='col-md-2'>
            <label>{{ $label }}</label>
        </div>
        <div class='col-md-10' style="word-break: break-word;">
            <label class="control-label hide-mobile">:</label>
            {{ $field }}
        </div>
    </div>
</div>