<table id="{{ $tname }}" class="table display stripe table-hover" width="100%">
    <thead>
        <tr>
            {{ $thead }}
        </tr>
    </thead>
</table>
@push('scriptsDocumentReady')
    var {{ $tname }} = $('#{{ $tname }}').DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        @if(isset($firstScript))
            {{ $firstScript }}
        @endif
        ajax:{
            "url": "{{ $url }}",
            "dataType": "json",
            "type": "GET",
            "timeout": 30000,
        },
        columns: [
            {{ $tbody }}
        ],
    });
    @if(!isset($no_number))
        {{ $tname }}.on('order.dt search.dt', function () {
            {{ $tname }}.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, {{ $tname }}) {
                cell.innerHTML = {{ $tname }} + 1;
            });
        }).draw();
    @endif
    @if(isset($secondScript))
        {{ $secondScript }}
    @endif
@endpush