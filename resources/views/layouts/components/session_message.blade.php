@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if(Session::has('success'))
    <p id="successMessage" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success') }}</p>
@endif
@if(Session::has('error'))
    <p id="successMessage" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
@endif
@if(Session::has('validator-error'))
    <p id="successMessage" class="alert {{ Session::get('alert-class', 'alert-danger') }}">
        @foreach (Session::get('validator-error')->all() as $error)
            {!! $error !!}<br>
        @endforeach
    </p>
@endif

