@extends('layouts.app')

@section('content-header')
    <h1>
        Company
        <small>Laravel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Company</h3>
        </div>
        <form method="POST" action="{{ route('companies.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                @include('layouts.components.session_message')
                @component('layouts.components.edit_one_column',['for' => 'name','required' => '1','label'=>'Name']) @slot('field')
                    <input type="text" name="name" value="{{old('name')}}" required maxlength="254" placeholder="Name">
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'email','required' => '0','label'=>'Email']) @slot('field')
                    <input type="email" name="email" value="{{old('email')}}" placeholder="Email">
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'logo','required' => '0','label'=>'Logo']) @slot('field')
                    <input type="file" name="logo" accept="image/jpeg, image/png">
                    <p class="help-block">File format allowed: jpeg, jpg, png</p>
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'website','required' => '0','label'=>'Website']) @slot('field')
                    <input type="text" name="website" value="{{old('website')}}" maxlength="254" placeholder="Website">
                @endslot @endcomponent
            </div>
            <div class='box-footer'>
                <div class='pull-right'>
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o">&nbsp;Save</i></button>
                </div>
                <button type="button" class="btn btn-danger pull-left" onClick="location.href ='{{route('companies.index')}}';"><i class="fa fa-undo"></i> Back</button>
            </div>
        </form>
    </div>
@endsection