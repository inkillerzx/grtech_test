@extends('layouts.app')

@section('content-header')
    <h1>
        Company
        <small>Laravel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Company</h3>
        </div>
        <div class="box-body">
            @include('layouts.components.session_message')
            @component('layouts.components.show_one_column',['label'=>'Name']) @slot('field')
                {{$company->name}}
            @endslot @endcomponent
            @component('layouts.components.show_one_column',['label'=>'Email']) @slot('field')
                {{$company->email}}
            @endslot @endcomponent
            @component('layouts.components.show_one_column',['label'=>'Logo']) @slot('field')
                @if(isset($company->logo))
                    <img style="width: 65px;height: 100%;" src="{{url('storage/'.$company->logo)}}">
                @else
                    Please Upload
                @endif
            @endslot @endcomponent
            @component('layouts.components.show_one_column',['label'=>'Website']) @slot('field')
                {{$company->website}}
            @endslot @endcomponent
        </div>
        <div class='box-footer'>
            <div class='pull-right'>
                <button type="button" onClick="location.href ='{{route('companies.edit',['company'=>$company->id])}}';" class="btn btn-primary"><i class="fa fa-pencil">&nbsp;Edit</i></button>
            </div>
            <button type="button" class="btn btn-danger pull-left" onClick="location.href ='{{route('companies.index')}}';"><i class="fa fa-undo"></i> Back</button>
        </div>
    </div>
@endsection