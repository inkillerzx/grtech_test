@extends('layouts.app')

@section('content-header')
    <h1>
        Companies
        <small>Laravel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Companies</li>
    </ol>
@endsection

@section('main-content')
    @push('modals')
        @include('layouts.components.modal_delete')
    @endpush 
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Companies</h3>
        </div>
        <div class="box-body">
            @include('layouts.components.session_message')
            <div class="table-responsive no-padding" style="border: none;">
                @component('layouts.components.table_ajax', ['tname' => 'companies_table_ajax']) @slot('url')
                    {{ route('companies.index')}}
                @endslot @slot('thead')
                    <th style='width: 30px;'>No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Logo</th>
                    <th>Website</th>
                    <th style="width:66px;">Action</th>
                @endslot @slot('tbody')
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'logo', name: 'logo' },
                    { data: 'website', name: 'website' },
                    { data: 'action', name: 'action' },
                @endslot @slot('firstScript')
                    dom: '<"row "<"col-sm-4"<B> ><"col-sm-4" f ><"col-sm-4" l>>rtip',
                    columnDefs: [{ orderable: false, targets: [5] }],
                    buttons: [
                        {
                            className: 'btn-sm btn-info',
                            text: '<i class="fa fa-plus"></i> Create Company',
                            action: function ( e, dt, node, config ) {
                                window.location = '{{ route('companies.create') }}';
                            }
                        },
                    ],
                @endslot @slot('secondScript')
                @endslot @endcomponent  
            </div>
        </div>
    </div>
@endsection  