@extends('layouts.app')

@section('content-header')
    <h1>
        Employees
        <small>Laravel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employees</li>
    </ol>
@endsection

@section('main-content')
    @push('modals')
        @include('layouts.components.modal_delete')
    @endpush 
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Employees</h3>
        </div>
        <div class="box-body">
            @include('layouts.components.session_message')
            <div class="table-responsive no-padding" style="border: none;">
                @component('layouts.components.table_ajax', ['tname' => 'employees_table_ajax']) @slot('url')
                    {{ route('employees.index')}}
                @endslot @slot('thead')
                    <th style='width: 30px;'>No.</th>
                    <th>Full Name</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style="width:66px;">Action</th>
                @endslot @slot('tbody')
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'full_name', name: 'full_name' },
                    { data: 'company', name: 'company' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'action', name: 'action' },
                @endslot @slot('firstScript')
                    dom: '<"row "<"col-sm-4"<B> ><"col-sm-4" f ><"col-sm-4" l>>rtip',
                    columnDefs: [{ orderable: false, targets: [5] }],
                    buttons: [
                        {
                            className: 'btn-sm btn-info',
                            text: '<i class="fa fa-plus"></i> Create Employee',
                            action: function ( e, dt, node, config ) {
                                window.location = '{{ route('employees.create') }}';
                            }
                        },
                    ],
                @endslot @slot('secondScript')
                @endslot @endcomponent  
            </div>
        </div>
    </div>
@endsection  