@extends('layouts.app')

@section('content-header')
    <h1>
        Employee
        <small>Laravel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employee</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Employee</h3>
        </div>
        <div class="box-body">
            @include('layouts.components.session_message')
            @component('layouts.components.show_one_column',['label'=>'Full Name']) @slot('field')
                {{$employee->full_name}}
            @endslot @endcomponent
            @component('layouts.components.show_one_column',['label'=>'Company']) @slot('field')
                {{$employee->companyIdCompany->name ?? null}}
            @endslot @endcomponent
            @component('layouts.components.show_one_column',['label'=>'Email']) @slot('field')
                <a href='mailto:{{$employee->email}}'>{{$employee->email}}</a>
            @endslot @endcomponent
            @component('layouts.components.show_one_column',['label'=>'Phone']) @slot('field')
                {{$employee->phone}}
            @endslot @endcomponent
        </div>
        <div class='box-footer'>
            <div class='pull-right'>
                <button type="button" onClick="location.href ='{{route('employees.edit',['employee'=>$employee->id])}}';" class="btn btn-primary"><i class="fa fa-pencil">&nbsp;Edit</i></button>
            </div>
            <button type="button" class="btn btn-danger pull-left" onClick="location.href ='{{route('employees.index')}}';"><i class="fa fa-undo"></i> Back</button>
        </div>
    </div>
@endsection