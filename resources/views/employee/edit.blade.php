@extends('layouts.app')

@section('content-header')
    <h1>
        Employee
        <small>Laravel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employee</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Employee</h3>
        </div>
        <form method="POST" action="{{ route('employees.update',['employee'=>$employee->id]) }}">
            @csrf
            @method('PUT')
            <div class="box-body">
                @include('layouts.components.session_message')
                @component('layouts.components.edit_one_column',['for' => 'first_name','required' => '1','label'=>'First Name']) @slot('field')
                    <input type="text" name="first_name" required value="{{old('first_name') ?? $employee->first_name}}" maxlength="254" placeholder="First Name">
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'last_name','required' => '1','label'=>'Last Name']) @slot('field')
                    <input type="text" name="last_name" required value="{{old('last_name') ?? $employee->last_name}}" maxlength="254" placeholder="Last Name">
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'company','required' => '0','label'=>'Company']) @slot('field')
                    <select name="company"><option value="">Select Company</option>
                        @foreach (\App\Company::pluck('name', 'id') as $key => $value)
                          <option value="{{ $key }}" {{ ($key == (old('company') ?? $employee->company)) ? 'selected' : '' }}> 
                              {{ $value }} 
                          </option>
                        @endforeach
                    </select>
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'email','required' => '0','label'=>'Email']) @slot('field')
                    <input type="email" name="email" value="{{old('email') ?? $employee->email}}" placeholder="Email">
                @endslot @endcomponent
                @component('layouts.components.edit_one_column',['for' => 'phone','required' => '0','label'=>'Phone']) @slot('field')
                    <input type="tel" name="phone" value="{{old('phone') ?? $employee->phone}}" maxlength="254" placeholder="Phone" pattern="^(\+?6?0)[0|1|2|3|4|6|7|8|9][0-9]{7,9}$">
                    <p class="help-block">Format: +601122224444</p>
                @endslot @endcomponent
            </div>
            <div class='box-footer'>
                <div class='pull-right'>
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o">&nbsp;Save</i></button>
                </div>
                <button type="button" class="btn btn-danger pull-left" onClick="location.href ='{{route('employees.index')}}';"><i class="fa fa-undo"></i> Back</button>
            </div>
        </form>
    </div>
@endsection