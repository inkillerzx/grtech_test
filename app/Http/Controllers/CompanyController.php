<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Request()->ajax()) {
            $modelDatatables = $this->ajax(Request(), 'index');
            return $modelDatatables->make(true);
        }
        return view('company.index');
    }
    
    public function ajax($request, $method) {
        if ($method == 'index') {
            $modelDatatables = Datatables::of(Company::query())
                    ->addIndexColumn()
                    ->editColumn('logo',function($query){
                        if(isset($query->logo)){
                            return '<img style="width: 65px;height: 100%;" src="'.url('storage/'.$query->logo).'">';
                        }
                    })
                    ->addColumn('action', function ($query) {
                        $btnView = "<a type='button' class='btn btn-xs btn-success' href='".route('companies.show',['company'=>$query->id])."'>View</a>";
                        $btnEdit = "<a type='button' class='btn btn-xs btn-primary' href='".route('companies.edit',['company'=>$query->id])."'>Edit</a>";
                        $btnDelete = "<a type='button' class='btn btn-xs btn-danger' href='#' data-modal data-route='" . route('companies.destroy', $query->id) . "' data-toggle='modal' data-target='#modal-delete'>Delete</a>";
                        return $btnView.' '.$btnEdit.' '.$btnDelete;
                    })->rawColumns(['logo', 'action']);
        }
        return $modelDatatables;
    }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'nullable|email',
            'logo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $input = $request->all();
        $company = new Company();
        $company->fill($input);
        if(isset($request->logo)){
            $filenameWithExt = time().'_'.$request->logo->getClientOriginalName();
            $request->logo->storeAs('public', $filenameWithExt);
            $company->logo = $filenameWithExt;
        }
        $company->save();
        \Session::flash('success', 'Successfully Created');
        return redirect(route('companies.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company) {
        return view('company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company) {
        return view('company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'nullable|email',
            'logo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $input = $request->all();
        if(isset($request->logo) && isset($company->logo)){
            Storage::delete('public/' . $company->logo);
        }
        $company->fill($input);
        if(isset($request->logo)){
            $filenameWithExt = time().'_'.$request->logo->getClientOriginalName();
            $request->logo->storeAs('public', $filenameWithExt);
            $company->logo = $filenameWithExt;
        }
        $company->save();
        \Session::flash('success', 'Successfully Updated');
        return redirect(back()->getTargetUrl());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company) {
        if(isset($company->logo)){
            Storage::delete('public/' . $company->logo);
        }
        $company->delete();
        \Session::flash('success', 'Successfully Deleted');
        return redirect(back()->getTargetUrl());
    }

}
