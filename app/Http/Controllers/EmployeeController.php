<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DataTables;

class EmployeeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Request()->ajax()) {
            $modelDatatables = $this->ajax(Request(), 'index');
            return $modelDatatables->make(true);
        }
        return view('employee.index');
    }

    public function ajax($request, $method) {
        if ($method == 'index') {
            $modelDatatables = Datatables::of(Employee::query())
                    ->addIndexColumn()
                    ->editColumn('company',function($query){
                        return $query->companyIdCompany->name ?? null;
                    })
                    ->addColumn('full_name',function($query){
                        return $query->full_name;
                    })
                    ->addColumn('action', function ($query) {
                        $btnView = "<a type='button' class='btn btn-xs btn-success' href='" . route('employees.show', ['employee' => $query->id]) . "'>View</a>";
                        $btnEdit = "<a type='button' class='btn btn-xs btn-primary' href='" . route('employees.edit', ['employee' => $query->id]) . "'>Edit</a>";
                        $btnDelete = "<a type='button' class='btn btn-xs btn-danger' href='#' data-modal data-route='" . route('employees.destroy', $query->id) . "' data-toggle='modal' data-target='#modal-delete'>Delete</a>";
                        return $btnView . ' ' . $btnEdit . ' ' . $btnDelete;
                    })
                    ->only(['full_name','company','email','phone','action'])->rawColumns(['logo', 'action']);
        }
        return $modelDatatables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'nullable|email',
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $input = $request->all();
        $employee = new Employee();
        $employee->fill($input);
        $employee->save();
        \Session::flash('success', 'Successfully Created');
        return redirect(route('employees.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee) {
        return view('employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee) {
        return view('employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'nullable|email',
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $input = $request->all();
        $employee->fill($input);
        $employee->save();
        \Session::flash('success', 'Successfully Updated');
        return redirect(route('employees.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee) {
        $employee->delete();
        \Session::flash('success', 'Successfully Deleted');
        return redirect(back()->getTargetUrl());
    }

}
